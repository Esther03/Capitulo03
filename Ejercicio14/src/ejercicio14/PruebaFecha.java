/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio14;

import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández Maaría Esther
 */
public class PruebaFecha {
    
    public static void main(String[] args) {
       Scanner s = new Scanner(System.in);
       int dia,mes,año;
       System.out.println("¿Que dia es?");
        dia = s.nextInt();
        System.out.println("¿Que mes es?");
        mes = s.nextInt();
        System.out.println("¿Que año es?");
        año = s.nextInt();
        Ejercicio14 fecha = new Ejercicio14(dia, mes, año);
        System.out.println("La fecha es: " + fecha);
    }
    
}
