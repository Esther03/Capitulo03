/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio17;
import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio17 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * (Aritmética: menor y mayor) Escriba una aplicación que reciba 
         * tres enteros del usuario y muestre la suma, promedio, producto, 
         * menor y mayor de esos números. Utilice las técnicas que se 
         * muestran en la figura 2.15 [nota: el cálculo del promedio en 
         * este ejercicio debe dar como resultado una representación entera 
         * del promedio. Por lo tanto, si la suma de los valores es 7, 
         * el promedio debe ser 2, no 2.3333...].
         */
        
        Scanner s=new Scanner(System.in);
        int n1, n2, n3, suma, promedio, producto;

        System.out.print("Escriba el primer número entero: ");
        n1=s.nextInt();
        System.out.print("Escriba el segundo número entero: ");
        n2=s.nextInt();
        System.out.print("Escriba el tercer número entero: ");
        n3=s.nextInt();
                      
        suma=n1+n2+n3;
        producto=(n1*n2)*n3;
        promedio=suma/3;
                     
                      
        System.out.printf("La suma es: %d%n", suma);
        System.out.printf("El producto es: %d%n", producto);
        System.out.printf("El promedio es: %d%n", promedio);
                      
        if(n1>n2 & n1>n3)
        System.out.printf("%d es el mayor de los tres%n", n1);
        if(n2>n1 & n2>n3)
        System.out.printf("%d es el mayor de los tres%n", n2);
        if(n3>n1 & n3>n2)
        System.out.printf("%d es el mayor de los tres%n", n3);
        if(n1 == n2 & n2 == n3)
        System.out.println("El primero = El segundo = El tercero");
        if(n1 != n2 & n2 ==n3)
        System.out.println("El segundo = El tercero");
        if(n1 == n2 & n2 != n3)
        System.out.println("El primero = El segundo");
        if(n1 !=n2 & n1 == n3)
        System.out.println("El primero = El tercero");
        if (n1 != n2 & n2 != n3)
        System.out.println("Todos los números son distintos\n");

    }
    
}
