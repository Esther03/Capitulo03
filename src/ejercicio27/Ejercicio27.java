/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio27;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio27 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * (Patrón de damas mediante asteriscos) Escriba una aplicación que 
         * muestre un patrón de tablero de damas, como se muestra a 
         * continuación:
         *       

* * * * * * * * 
 * * * * * * * * 
* * * * * * * *  
 * * * * * * * *  
* * * * * * * *  
 * * * * * * * * 
* * * * * * * *  
 * * * * * * * * 

         */
        
         System.out.print("********\n "
                + "********\n"
                + "********\n "
                + "********\n"
                + "********\n "
                + "********\n"
                + "********\n "
                + "********");

    }
    
}
