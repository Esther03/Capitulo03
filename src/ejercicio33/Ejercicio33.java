/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio33;
import java.util.Scanner;

/**
 *
 * @author arman
 */
public class Ejercicio33 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * (Calculadora del índice de masa corporal) En el ejercicio 1.10 introdujimos la calculadora del índice de masa corporal (BMI). Las fórmulas para calcular el BMI son
BMI = (pesoEnLibras × 703)/(alturaEnPulgadas × alturaEnPulgadas)
o
BMI = (pesoEnKilogramos)/(alturaEnMetros × alturaEnMetros)
Cree una calculadora del BMI que lea el peso del usuario en libras y la altura en pulgadas (o, si lo prefiere, el peso del usuario en kilogramos y la altura en metros), para que luego calcule y muestre el índice de masa corporal del usuario. 
Muestre además la siguiente información del Departamento de Salud y Servicios Humanos/Instituto Nacional de Salud para que el usuario pueda evaluar su BMI: 
Nota: en este capítulo aprendió a usar el tipo int para representar números enteros. Cuando se realizan los cálculos del BMI con valores int, se producen resultados en números enteros. En el capítulo 3 aprenderá a usar el tipo double para representar a los números con puntos decimales. Cuando se realizan los cálculos del BMI con valores double, producen números con puntos decimales; a éstos se les conoce como números de “punto flotante”].
         */
        Scanner s=new Scanner(System.in);
        float h, p, IMC;
           
        System.out.print("¿Cuál es su altura? (en metros): ");
        h=s.nextFloat();
        System.out.print("¿Cuál es su peso? (en kilogramos): ");
        p=s.nextFloat();
         
        IMC=p/(h*h);
        System.out.printf("Su Índice de Masa Corporal es: %f%n", IMC);
        
        if(IMC<18.5)
        System.out.printf("Usted tiene bajo peso, según la información"
                     + " del Departamento de Salud y Servicios Humanos/Instituto"
                     + " Nacional de Salud%n");
        if(IMC>=18.5 & IMC<=24.9)
        System.out.printf("Usted tiene peso normal, según la información "
                     + "del Departamento de Salud y Servicios Humanos/Instituto "
                     + "Nacional de Salud%n");
        if(IMC>=25 & IMC<=19.9)
        System.out.printf("Usted tiene sobrepeso, según la información "
                     + "del Departamento de Salud y Servicios Humanos/Instituto "
                     + "Nacional de Salud%n");
        if(IMC>=30)
        System.out.printf("Usted tiene obesidad, según la información"
                     + "del Departamento de Salud y Servicios Humanos/Instituto "
                     + "Nacional de Salud%n");

    }
    
}
