/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio34;
import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio34 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * (Calculadora del crecimiento de la población mundial) Use Web para determinar la población mundial actual y la tasa de crecimiento anual de la población mundial. Escriba una aplicación que reciba estos valores como entrada y luego muestre la población mundial estimada después de uno, dos, tres, cuatro y cinco años.
         */
        Scanner s=new Scanner(System.in);
        int población, p, año;
        double tasa;
       
        System.out.print("Antes de empezar:\nPor favor recuerde que en Java, la "
               + "variable de tipo int va de −2,147,483,648 a 2,147,483,647\n"
               + "\n¿Cuál es la población actual? ");
        población=s.nextInt();
        System.out.print("¿Cuál es la tasa de crecimiento de ese año? ");
        tasa=s.nextDouble();
       
        p=(int) (tasa*población/100);
       
        System.out.print("¿A cuántos años quieres hacer la estimación?\n"
               + "¿1, 2, 3, 4 o 5 años?\nQuiero hacer la estimación a: ");
        año=s.nextInt();
       
        if (año == 1)
           System.out.printf("Se estima que dentro de un año la población "
                   + "será: %d%n", p+población);
        if (año == 2)
           System.out.printf("Se estima que dentro de dos años la población "
                   + "será: %d%n", 2*p+población);
        if(año == 3)
           System.out.printf("Se estima que dentro de tres años la población "
                   + "será: %d%n", 3*p+población);
        if (año == 4)
           System.out.printf("Se estima que dentro de cuatro años la población "
                   + "será: %d%n", 4*p+población);
        if (año == 5)
           System.out.printf("Se estima que dentro de cinco años la población "
                   + "será: %d%n", 5*p+población);

    }
    
}
