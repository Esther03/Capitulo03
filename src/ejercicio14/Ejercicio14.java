/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio14;

/**
 *
 * @author arman
 */
public class Ejercicio14 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * Escriba una aplicación que muestre los números del 
         * 1 al 4 en la misma línea, con cada par de números adyacentes 
         * separado por un espacio. Use las siguientes técnicas:
         */
        //Mediante una instrucción System.out.println
        System.out.println("12 34");
        //Mediante cuatro instrucciones System.out.print.
        System.out.print("12 34\n");
        //Mediante una instrucción System.out.printf
        System.out.printf("%s%s %s%s%n", "1", "2", "3", "4");
    }
    
}
