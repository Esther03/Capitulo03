/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio11;

import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class PruebaCuenta {
    public static void main(String[] args){
        
        Scanner s=new Scanner (System.in);
        // crea un objeto Scanner para obtener la entrada de la ventana de comandos
        Scanner entrada = new Scanner(System.in);
        int op=0;
        Ejercicio11 cuenta1 = new Ejercicio11("Jane Green", 50.00);
        Ejercicio11 cuenta2 = new Ejercicio11("John Blue", -7.53);
        
        System.out.println("¿Qué quiere hacer?\n 1) Depositar\n 2)Retirar");
        while(op !=2){
            op=s.nextInt();
            
            switch(op){
                
            case 1:
                

                // muestra el saldo inicial de cada objeto
                System.out.printf("Saldo de %s: $%.2f%n",
                cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
                System.out.printf("Saldo de %s: $%.2f%n%n",
                cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());

                System.out.print("Escriba el monto a depositar para cuenta1: "); // indicador (promt)
                double montoDeposito = entrada.nextDouble(); // obtiene entrada del usuario
                System.out.printf("%nsumando %.2f al saldo de cuenta1%n%n", montoDeposito);
                cuenta1.depositar(montoDeposito); // suma al saldo de cuenta1
                // muestra los saldos
                System.out.printf("Saldo de %s: $%.2f%n",
                cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
                System.out.printf("Saldo de %s: $%.2f%n%n",
                cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());

                System.out.print("Escriba el monto a depositar para cuenta2: "); // indicador (promt)
                montoDeposito = entrada.nextDouble(); // obtiene entrada del usuario
                System.out.printf("%nsumando %.2f al saldo de cuenta2%n%n",
                montoDeposito);
                cuenta2.depositar(montoDeposito); // suma al saldo de cuenta2
                
                break;
                
            case 2:
                
                // muestra los saldos
                System.out.printf("Saldo de %s: $%.2f%n",
                cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
                System.out.printf("Saldo de %s: $%.2f%n%n",
                cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());
                

                System.out.print("Escriba el monto a retirar para cuenta1: "); // indicador (promt)
                montoDeposito = entrada.nextDouble(); // obtiene entrada del usuario
                System.out.printf("%nrestando %.2f al saldo de cuenta1%n%n", montoDeposito);
                cuenta1.retirar(montoDeposito); // suma al saldo de cuenta1
                // muestra los saldos
                System.out.printf("Saldo de %s: $%.2f%n",
                cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
                System.out.printf("Saldo de %s: $%.2f%n%n",
                cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());

                System.out.print("Escriba el monto a retirar para cuenta2: "); // indicador (promt)
                montoDeposito = entrada.nextDouble(); // obtiene entrada del usuario
                System.out.printf("%nrestando %.2f al saldo de cuenta2%n%n",
                montoDeposito);
                cuenta2.retirar(montoDeposito); // suma al saldo de cuenta2

                // muestra los saldos
                System.out.printf("Saldo de %s: $%.2f%n",
                cuenta1.obtenerNombre(), cuenta1.obtenerSaldo());
                System.out.printf("Saldo de %s: $%.2f%n%n",
                cuenta2.obtenerNombre(), cuenta2.obtenerSaldo());
                break;

            }
    
        }
    }
}
