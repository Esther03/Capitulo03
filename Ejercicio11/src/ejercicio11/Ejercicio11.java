/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio11;

/**3.11 (Clase Cuenta modificada) Modifique la clase Cuenta (figura 3.8) para proporcionar un método llamado
retirar, que retire dinero de un objeto Cuenta. Asegúrese de que el monto a retirar no exceda el saldo de Cuenta. Si
lo hace, el saldo debe permanecer sin cambio y el método debe imprimir un mensaje que indique “El monto a retirar
excede el saldo de la cuenta”. Modifique la clase PruebaCuenta (figura 3.9) para probar el método retirar.
*/

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio11 {
    
    private String nombre; // variable de instancia
    private double saldo; // variable de instancia
    
    // Constructor de Cuenta que recibe dos parámetros
    public Ejercicio11(String nombre, double saldo){
    this.nombre = nombre; // asigna nombre a la variable de instancia nombre

    // valida que el saldo sea mayor que 0.0; de lo contrario,
    // la variable de instancia saldo mantiene su valor inicial predeterminado de 0.0
    if (saldo > 0.0) // si el saldo es válido
    this.saldo = saldo; // lo asigna a la variable de instancia saldo
    }

    // método que deposita (suma) sólo una cantidad válida al saldo
    public void depositar(double montoDeposito){
    if (montoDeposito > 0.0) // si el montoDeposito es válido
    saldo = saldo + montoDeposito; // lo suma al saldo
    }
    
    //método que retira (resta) sólo una cantidad validad al saldo
    public void retirar(double montoDeposito){
        if (montoDeposito >0.0 && montoDeposito < saldo)
            saldo=saldo-montoDeposito;
        if (montoDeposito < 0.0 || montoDeposito > saldo)
            System.out.println("El monto a retirar excede el saldo de "
                    + "la cuenta");
    }

    // método que devuelve el saldo de la cuenta
    public double obtenerSaldo(){
        return saldo;
    }

    // método que establece el nombre
    public void establecerNombre(String nombre){
        this.nombre = nombre;
    }

    // método que devuelve el nombre
    public String obtenerNombre(){
        return nombre; //devuelve el valor de name a quien lo invocó
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
