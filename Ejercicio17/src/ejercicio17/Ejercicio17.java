/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio17;

/**
 * 3.17 (Computarización de los registros médicos) Un tema relacionado con la salud que ha estado últimamente
en las noticias es la computarización de los registros médicos. Esta posibilidad se está tratando con mucho cuidado,
debido a las delicadas cuestiones de privacidad y seguridad, entre otras cosas. [Trataremos esas cuestiones en ejercicios
posteriores]. La computarización de los registros médicos puede facilitar a los pacientes el proceso de compartir sus
perfiles e historiales médicos con los diversos profesionales de la salud que consulten. Esto podría mejorar la calidad
del servicio médico, ayudar a evitar conflictos de fármacos y prescripciones erróneas, reducir los costos y, en emergencias,
podría ayudar a salvar vidas. 
En este ejercicio usted diseñará una clase “inicial” llamada PerfilMedico para un persona. Los atributos de la clase deben 
incluir el primer nombre de la persona, su apellido, sexo, fecha de nacimiento
(que debe consistir de atributos separados para el día, mes y año de nacimiento), altura (en centímetros) y peso (en
kilogramos). Su clase debe tener un constructor que reciba estos datos. Para cada atributo, debe proveer los métodos
establecer y obtener. La clase también debe incluir métodos que calculen y devuelvan la edad del usuario en años, la
frecuencia cardiaca máxima y el rango de frecuencia cardiaca esperada (vea el ejercicio 3.16), además del índice de masa
corporal (BMI; vea el ejercicio 2.33). Escriba una aplicación de Java que pida la información de la persona, cree una
instancia de un objeto de la clase PerfilMedico para esa persona e imprima la información de ese objeto (incluyendo
el primer nombre de la persona, apellido, sexo, fecha de nacimiento, altura y peso), y que después calcule e imprima
la edad de esa persona en años, junto con el BMI, la frecuencia cardiaca máxima y el rango de frecuencia cardiaca esperada.
También debe mostrar la tabla de valores del BMI del ejercicio 2.33.
 */

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio17 {
    
    String Nombre;
    String Apellido;
    String Sexo;
    short día;
    short mes;
    int año;
    double altura;
    double peso;
    
    public Ejercicio17(String Nombre, String Apellido, String sexo,
            short día, short mes, int año, double altura, double peso){
        this.Nombre=Nombre;
        this.Apellido=Apellido;
        this.día=día;
        this.mes=mes;
        this.año=año;
        this.Sexo=sexo;
        this.altura=altura;
        this.peso=peso;
        
    }
    
    public void establecerNombre(String Nombre){
        this.Nombre = Nombre;
    }

    public String obtenerNombre(){
        return Nombre; 
    }
    
    public void establecerApellido(String Apellido){
        this.Apellido = Apellido;
    }

    public String obtenerApellido(){
        return Apellido; 
    }
    
    public void establecerDia(short día){
        this.día = día;
    }

    public short obtenerDia(){
        return día; 
    }
    
    public void establecerMes(short mes){
        this.mes=mes;
    }

    public short obtenerMes(){
        return mes; 
    }
    
    public void establecerAño(int año){
        this.año= año;
    }

    public int obtenerAño(){
        return año; 
    }
    
    public void establecerSexo(String sexo){
        this.Sexo=sexo;
    }
    
    public String obtenerSexo(){
        return Sexo;
    }
    
    public void establecerAltura(double altura){
        this.altura=altura;
    }
    
    public double obtenerAltura(){
        return altura;
    }
    
    public void establecerPeso(double peso){
        this.peso=peso;
    }
    
    public double obtenerPeso(){
        return peso;
    }
    
    public void Paciente(){
        
        System.out.printf("%nFecha en que ingresó el Paciente: 31 Diciembre 2018%n"
                + "%nPaciente: %s %s%nFecha de nacimiento: %s/%s/%s"
                + "%nSexo: %s%nAltura: %s%nPeso: %s%n",
                obtenerApellido(), obtenerNombre(), obtenerDia(), obtenerMes(),
                obtenerAño(), obtenerSexo(), obtenerAltura(), obtenerPeso());
        
        if(obtenerDia() < 0 || obtenerDia() > 31)
        System.out.print("Error con la fecha de nacimiento.");
        if(obtenerMes() < 0 || obtenerMes() > 12)
        System.out.print("Errror con la fecha de nacimiento.");
        if(obtenerAño() > 2018)
        System.out.print("Error con la fecha de nacimiento.");
        
    }
    
    public void EdadPaciente(){
        
        int EdadPaciente=2018-obtenerAño();
        
        System.out.printf("%nLa edad del paciente %s es: %s%n", obtenerApellido(),
                EdadPaciente);
        
        if(obtenerDia() < 0 || obtenerDia() > 31)
        System.out.print("Error con la fecha de nacimiento.");
        if(obtenerMes() < 0 || obtenerMes() > 12)
        System.out.print("Errror con la fecha de nacimiento.");
        if(obtenerAño() > 2018)
        System.out.print("Error con la fecha de nacimiento.");
        
    }
    
    public void FrecuenciaCarciadaMax(){
        int EdadPaciente=obtenerAño()-2018;
        int FrecuenciaCardiacaMax=220-EdadPaciente;
        
        System.out.printf("%nLa frecuencia cardiaca máxima del paciente en pulsos por minuto"
                + "es de: %s%n (De acuerdo con la Asociación Estadounidense del Corazón (AHA))%n",
                FrecuenciaCardiacaMax);
        if(obtenerDia() < 0 || obtenerDia() > 31)
        System.out.print("Error con la fecha de nacimiento.");
        if(obtenerMes() < 0 || obtenerMes() > 12)
        System.out.print("Errror con la fecha de nacimiento.");
        if(obtenerAño() > 2018)
        System.out.print("Error con la fecha de nacimiento.");
    }
    
    
    
    public void FrecuenciaCardiacaESp(){
        int EdadPaciente=obtenerAño()-2018;
        int FrecuenciaCardiacaMax=220-EdadPaciente;
        double FrecuenciaCardiacaEsp=67.5*FrecuenciaCardiacaMax/100;
        
        System.out.printf("%nLa frecuencia cardiaca esperada del paciente %s en pulsos por minuto"
                + "es de: %.2f%n "
                + "(La frecuencia cardiaca esperada se encuentra entre el 50 y 85 "
                + "porciento de la frecuencia cardiaca máxima, "
                + "por lo que tomaremos el 65.7 porciento. "
                + "De acuerdo con la Asociación Estadounidense del Corazón (AHA))%n",
                obtenerApellido(),FrecuenciaCardiacaEsp);
        
        if(obtenerDia() < 0 || obtenerDia() > 31)
        System.out.print("Error con la fecha de nacimiento.");
        if(obtenerMes() < 0 || obtenerMes() > 12)
        System.out.print("Errror con la fecha de nacimiento.");
        if(obtenerAño() > 2018)
        System.out.print("Error con la fecha de nacimiento.");
    }
    
    public void CalculoIMC(){
        double IMC; 
        
        double alturaM=obtenerAltura()*0.01;
        
        IMC=obtenerPeso()/(alturaM*alturaM);
        System.out.printf("Su Índice de Masa Corporal es: %f%n", IMC);
        
        if(IMC<18.5)
        System.out.printf("Usted tiene bajo peso, según la información"
                     + " del Departamento de Salud y Servicios Humanos/Instituto"
                     + " Nacional de Salud%n");
        if(IMC>=18.5 & IMC<=24.9)
        System.out.printf("Usted tiene peso normal, según la información "
                     + "del Departamento de Salud y Servicios Humanos/Instituto "
                     + "Nacional de Salud%n");
        if(IMC>=25 & IMC<=19.9)
        System.out.printf("Usted tiene sobrepeso, según la información "
                     + "del Departamento de Salud y Servicios Humanos/Instituto "
                     + "Nacional de Salud%n");
        if(IMC>=30)
        System.out.printf("Usted tiene obesidad, según la información"
                     + "del Departamento de Salud y Servicios Humanos/Instituto "
                     + "Nacional de Salud%n");
    }


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
