/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio13;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class PruebaEmpleado {
    
    public static void main(String [] args){
        Ejercicio13 Empleado1=new Ejercicio13("John", "Smith", 1528.80);
        Ejercicio13 Empleado2=new Ejercicio13("Maggy", "Mendoza", 1577.70);
        
        System.out.printf("El Empleado1 se llama %s %s y su saldo mensual es de %.2f%n",
                Empleado1.obtenerNombre(), Empleado1.obtenerApellidoP(), 
                Empleado1.obtenerSalario());
        System.out.printf("El Empleado2 se llama %s %s y su saldo mensual es de %s%n%n",
                Empleado2.obtenerNombre(), Empleado2.obtenerApellidoP(), 
                Empleado2.obtenerSalario());
        
        Empleado1.ObtenerSalarioAnual();
        Empleado2.ObtenerSalarioAnual();
        
        System.out.printf("%nSe le da un aumneto del 10 porciento al señor %s"
                + " y a la señorita %s%n", 
                Empleado1.obtenerApellidoP(), Empleado2.obtenerApellidoP());
        System.out.printf("%nCalculado el salario anual después del aumento...%n");
        Empleado1.AumentoSalario();
        Empleado2.AumentoSalario();
        
    }
    
}
