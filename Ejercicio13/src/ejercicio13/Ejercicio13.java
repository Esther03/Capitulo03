/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio13;

/**
 * 3.13 (La clase Empleado) Cree una clase llamada Empleado, que incluya tres variables de instancia: un primer
nombre (tipo String), un apellido paterno (tipo String) y un salario mensual (double). Su clase debe tener un constructor
que inicialice las tres variables de instancia. Proporcione un método establecer y un método obtener para cada
variable de instancia. Si el salario mensual no es positivo, no establezca su valor. Escriba una aplicación de prueba
llamada PruebaEmpleado, que demuestre las capacidades de la clase Empleado. Cree dos objetos Empleado y muestre el
salario anual de cada objeto. Después, proporcione a cada Empleado un aumento del 10% y muestre el salario anual
de cada Empleado otra vez.
 */

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio13 {
    
    String PrimerNombre;
    String ApellidoP;
    double salarioM;
    
    public Ejercicio13(String PrimerNombre, String ApellidoP, double salarioM){
        this.PrimerNombre=PrimerNombre;
        this.ApellidoP=ApellidoP;
        
        if(salarioM > 0.0)
            this.salarioM=salarioM;
    }
    
    // método que establece el nombre
    public void establecerPrimerNombre(String PrimerNombre){
        this.PrimerNombre = PrimerNombre;
    }

    // método que devuelve el nombre
    public String obtenerNombre(){
        return PrimerNombre; 
    }
    
    // método que establece el apellido Paterno
    public void establecerApellidoP(String ApellidoP){
        this.ApellidoP = ApellidoP;
    }

    // método que devuelve el apellido Paterno
    public String obtenerApellidoP(){
        return ApellidoP; 
    }
    
    // método que establece el salario
    public void establecerSalarioM(double SalarioM){
        this.salarioM = SalarioM;
    }

    // método que devuelve el salario
    public double obtenerSalario(){
        return salarioM; 
    }
    
    public void ObtenerSalarioAnual(){
        if (obtenerSalario() < 0.0)
            salarioM = 0.0;
        
        System.out.printf("El salario anual de %s es: %.2f%n", 
                obtenerNombre(), obtenerSalario()*12);
    }
    
    public void AumentoSalario(){
        if(obtenerSalario() < 0.0)
            salarioM=0.0;
        
                
        double salario10=((10*obtenerSalario())/100);
        double salario=obtenerSalario()+salario10;
        
        System.out.printf("El salario anual con el aumento de %s es: %.2f%n",
                obtenerNombre(), salario*12);
    }
            

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
