/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio12;

import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class PruebaFactura {
    
    public static void main(String[] args){
        Ejercicio12 Factura=new Ejercicio12("01", "Ninguna", -12, 40.0);
        
        //Muestra el estado actual de la factura
        System.out.println("Factura de la Ferretería\n");
        System.out.printf("Número de Pieza: %s%n",Factura.obtenerNoDePieza());
        System.out.printf("Decripción de la pieza: %s%n", 
                Factura.obtenerDescripcion());
        System.out.printf("Cantidad: %s%n", Factura.obtenerCantidad());
        System.out.printf("Precio unitario: %s%n", Factura.obtenerPrecio());
        System.out.printf("Monto a pagar es: %s%n", 
                Factura.obtenerCantidad()*Factura.obtenerPrecio());
        
        // crea un objeto Scanner para obtener la entrada de la ventana de comandos
        Scanner entrada = new Scanner(System.in);
        
        System.out.println("\nEscriba la cantidad de piezas que compró: ");
        int Cantidad=entrada.nextInt();
        
        System.out.println("\nImprimiendo Facutra...\n");
        Factura.ObtenerMontoFactura(Cantidad);
    }
    
}
