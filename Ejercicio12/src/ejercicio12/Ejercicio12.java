/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio12;

/**
 * 3.12 (La clase Factura) Cree una clase llamada Factura que una ferretería podría utilizar para representar una
factura para un artículo vendido en la tienda. Una Factura debe incluir cuatro piezas de información como variables
de instancia: un número de pieza (tipo String), la descripción de la pieza (tipo String), la cantidad de artículos de ese
tipo que se van a comprar (tipo int) y el precio por artículo (double). Su clase debe tener un constructor que inicialice
las cuatro variables de instancia. Proporcione un método establecer y un método obtener para cada variable de
instancia. Además, proporcione un método llamado obtenerMontoFactura, que calcule el monto de la factura (es
decir, que multiplique la cantidad de artículos por el precio de cada uno) y después devuelva ese monto como un valor
double. Si la cantidad no es positiva, debe establecerse en 0. Si el precio por artículo no es positivo, debe establecerse
en 0.0. Escriba una aplicación de prueba llamada PruebaFactura, que demuestre las capacidades de la clase Factura.
 */

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio12 {
    
    private String NoDePieza; // Número de la pieza
    private String Descripcion; // Descripción de la pieza
    private int Cantidad; // Cantidad del artículo que va a comprar
    private double precio; // Valor del artículo
    
    public Ejercicio12(String NoDePieza, String Descripcion, int Cantidad, 
            double Precio){
        
        this.NoDePieza= NoDePieza;
        this.Descripcion=Descripcion;
        
        if(Cantidad > 0)
            this.Cantidad=Cantidad;
        if(Precio > 0.0)
            this.precio=Precio;
        
    }
    
    // método que establece el nombre
    public void establecerNoDePirza(String NoDePieza){
        this.NoDePieza = NoDePieza;
    }

    // método que devuelve el nombre
    public String obtenerNoDePieza(){
        return NoDePieza; //devuelve el valor de name a quien lo invocó
    }
    
    // método que establece el nombre
    public void establecerDescripcion(String Descripcion){
        this.Descripcion = Descripcion;
    }

    // método que devuelve el nombre
    public String obtenerDescripcion(){
        return Descripcion; //devuelve el valor de name a quien lo invocó
    }
    
    // método que establece el nombre
    public void establecerCantidad(int Cantidad){
        this.Cantidad = Cantidad;
    }

    // método que devuelve el precio
    public int obtenerCantidad(){
        return Cantidad; //devuelve el valor de name a quien lo invocó
    }
    
    // método que establece el precio
    public void establecerPrecio(double Precio){
        this.precio = Precio;
    }

    // método que devuelve el precio
    public double obtenerPrecio(){
        return precio; //devuelve el valor de name a quien lo invocó
    }
    
    // método que muestra la factura
    public void ObtenerMontoFactura(int Cantidad){
        if ( Cantidad < 0) 
            Cantidad=0; 
        if (precio < 0.0)
            precio=0.0;
        
        System.out.println("Factura de la Ferretería");
        System.out.printf("Número de Pieza: %s%n",obtenerNoDePieza());
        System.out.printf("Decripción de la pieza: %s%n", obtenerDescripcion());
        System.out.printf("Cantidad: %s%n", Cantidad);
        System.out.printf("Precio unitario: %s%n", obtenerPrecio());
        System.out.printf("Monto a pagar es: %s%n", Cantidad*obtenerPrecio());
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
